﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetFeed.Business.Interfaces;

namespace TweetFeed.Business.Models
{
	public class User : IPerson
	{
		public List<Follower> FollowerList { get; set; }

		public List<string> Tweets { get; set; }

		public string Name { get; set; }
	}
}
