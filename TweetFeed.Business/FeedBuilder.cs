﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetFeed.Business.Models;

namespace TweetFeed.Business
{
	public static class FeedBuilder
	{
		private const string separatorValue = "follows";
		private static int followersIndex { get; set; }
		public static string userNameComparer { get; set; }

		public static List<User> InitialiseFeed(string[] userData, string[] tweetData)
		{
			List<User> users = new List<User>();
			string userNameComparer = string.Empty;
			for (int i = 0; i < userData.Length; i++)
			{
				userNameComparer = userData[i].Split(' ')[0].Trim();
				AddUser(users, userNameComparer);

				followersIndex = userData[i].IndexOf(separatorValue) + separatorValue.Length + 1;
				foreach (var item in userData[i].Substring(followersIndex).Split(','))
				{
					userNameComparer = item.Trim();
					AddUser(users, userNameComparer);
				}
			}
			List<User> usersWithFollowers = AddUserFollowers(userData, tweetData, users);
			return usersWithFollowers.OrderBy(u => u.Name).ToList();
		}

		private static void AddUser(List<User> users, string userNameComparer)
		{
			if (!users.Exists(u => u.Name == userNameComparer))
			{
				User user = new User();
				user.FollowerList = new List<Follower>();
				user.Name = userNameComparer;
				users.Add(user);
			}
		}

		private static List<User> AddUserFollowers(string[] userData, string[] tweetData, List<User> users)
		{
			foreach (var user in users)
			{
				foreach (var userDataItem in userData.ToList())
				{
					if (user.Name == userDataItem.Split(' ')[0].Trim())
					{
						followersIndex = userDataItem.IndexOf(separatorValue) + separatorValue.Length + 1;
						var followers = userDataItem.Substring(followersIndex).Split(',');
						foreach (var follower in followers)
						{
							if (!user.FollowerList.Exists(f => f.Name == follower.Trim()))
							{
								Follower userFollower = new Follower();
								userFollower.Name = follower.Trim();
								user.FollowerList.Add(userFollower);
							}
						}
					}
				}
			}
			List<User> usersWithTweets = MapUserTweets(tweetData, users);
			return usersWithTweets;
		}

		private static List<User> MapUserTweets(string[] tweetData, List<User> users)
		{
			foreach (var user in users)
			{
				user.Tweets = new List<string>();
				foreach (var item in tweetData.ToList())
				{
					userNameComparer = item.Split('>')[0].Trim();
					AddUserTweet(user, item);

					foreach (var userFollower in user.FollowerList)
					{
						AddFollowerTweet(user, item, userFollower);
					}
				}
			}
			return users;
		}

		private static void AddFollowerTweet(User user, string item, Follower userFollower)
		{
			if (userFollower.Name == userNameComparer)
			{
				user.Tweets.Add(string.Format("\t@{0}: {1}\n", userFollower.Name, item.Split('>')[1].Trim()));
			}
		}

		private static void AddUserTweet(User user, string item)
		{
			if (user.Name == userNameComparer)
			{
				user.Tweets.Add(string.Format("\t@{0}: {1}\n", user.Name, item.Split('>')[1].Trim()));
			}
		}
	}
}
