﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TweetFeed.Business.Interfaces
{
	public interface IPerson
	{
		List<string> Tweets { get; set; }

		string Name {get; set;}
	}
}
