﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetFeed.Business.Models;

namespace TweetFeed.Business
{
	public static class DataHandler
	{
		public static string[] ReadDataFile(string fileName, string fileLocation)
		{
			string[] data = File.ReadAllLines(string.Format("{0}\\{1}", fileLocation, fileName));
			return data;
		}
	}
}
