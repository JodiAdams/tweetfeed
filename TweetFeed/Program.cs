﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetFeed.Business;
using TweetFeed.Business.Models;

namespace TweetFeed
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				string userFileName = "user.txt";
				string tweetsFileName = "tweet.txt";
				string userFilePath = string.Empty;
				string tweetsFilePath = string.Empty;
				string fileDoesNotExistMessage = string.Format("Please check if the resource files \"{0}\" and \"{1}\" exist in the specified location.", userFileName, tweetsFileName);
				string filesLocation = string.Empty;

				while (true)
				{
					Console.WriteLine("Please enter the location of the source files.");
					filesLocation = Console.ReadLine();
					if (filesLocation == "exit")
					{
						break;
					}
					userFilePath = string.Format("{0}\\{1}", filesLocation, userFileName);
					tweetsFilePath = string.Format("{0}\\{1}", filesLocation, tweetsFileName);
					if (!(File.Exists(userFilePath) && File.Exists(tweetsFilePath)))
					{
						Console.WriteLine(fileDoesNotExistMessage);
					}
					else
					{
						string[] userData = DataHandler.ReadDataFile(userFileName, filesLocation);
						string[] tweetData = DataHandler.ReadDataFile(tweetsFileName, filesLocation);
						List<User> users = new List<User>();

						users = FeedBuilder.InitialiseFeed(userData, tweetData);
						DisplayFeed(users);
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(string.Format("Error : {0}\r\n InnerException:\r\n {1} Trace:\r\n {2}", ex.Message, ex.InnerException, ex.StackTrace));
			}
		}

		private static void DisplayFeed(List<User> users)
		{
			foreach (var userItem in users)
			{
				Console.WriteLine(userItem.Name + "\n");
				foreach (var item in userItem.Tweets)
				{
					Console.WriteLine(item);
				}
			}
		}
	}
}
